package com.uni.jmalpv.ef;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TriviaAdapter triviaAdapter;
    ArrayList<Integer> image = new ArrayList<>();
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Boolean> answer = new ArrayList<>();

    int position = 0;

    ImageView imageView;
    TextView textView;
    Button buttonTrue, buttonFalse, buttonNext, buttonPrevious;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image_view_trivia);
        textView = findViewById(R.id.text_view_trivia);

        buttonTrue = findViewById(R.id.button_true);
        buttonFalse = findViewById(R.id.button_false);
        buttonNext = findViewById(R.id.button_next);
        buttonPrevious = findViewById(R.id.button_previous);

        fillArray();
        triviaAdapter = new TriviaAdapter(this, image, text, answer);
        TriviaInitialization();

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position +=1;
                if(position >= triviaAdapter.getCount()) {
                    position = 0;
                }
                imageView.setImageResource(triviaAdapter.image.get(position));
                textView.setText(triviaAdapter.text.get(position));
            }
        });

        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position -=1;
                if(position < 0) {
                    position = 0;
                }
                imageView.setImageResource(triviaAdapter.image.get(position));
                textView.setText(triviaAdapter.text.get(position));
            }
        });

        buttonTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answer.get(position) == true){
                    Snackbar.make(view, R.string.text_correct, Snackbar.LENGTH_LONG).show();
                    return;
                }
                Snackbar.make(view, R.string.text_incorrect, Snackbar.LENGTH_LONG).show();
            }
        });

        buttonFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answer.get(position) == false){
                    Snackbar.make(view, R.string.text_correct, Snackbar.LENGTH_LONG).show();
                    return;
                }
                Snackbar.make(view, R.string.text_incorrect, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void fillArray(){
        image.add(R.drawable.a);
        image.add(R.drawable.b);
        image.add(R.drawable.c);
        image.add(R.drawable.d);
        image.add(R.drawable.e);
        image.add(R.drawable.f);
        image.add(R.drawable.g);
        image.add(R.drawable.h);
        image.add(R.drawable.i);
        image.add(R.drawable.j);

        Resources res = getResources();
        String[] items = res.getStringArray(R.array.trivia_array);
        for(int i=0; i<items.length; i++){
            text.add(items[i]);
        }

        answer.add(Boolean.FALSE);
        answer.add(Boolean.TRUE);
        answer.add(Boolean.TRUE);
        answer.add(Boolean.FALSE);
        answer.add(Boolean.FALSE);
        answer.add(Boolean.TRUE);
        answer.add(Boolean.TRUE);
        answer.add(Boolean.FALSE);
        answer.add(Boolean.FALSE);
        answer.add(Boolean.TRUE);
    }

    private void TriviaInitialization(){
        imageView.setImageResource(triviaAdapter.image.get(position));
        textView.setText(triviaAdapter.text.get(position));
    }
}
package com.uni.jmalpv.ef;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TriviaAdapter  extends BaseAdapter {

    Context context;
    ArrayList<Integer> image;
    ArrayList<String> text;
    ArrayList<Boolean> answer;

    public TriviaAdapter(Context context, ArrayList<Integer> image, ArrayList<String> text, ArrayList<Boolean> answer) {

        this.context = context;
        this.image = image;
        this.text = text;
        this.answer = answer;
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main, parent, false);
        ImageView imageView = view.findViewById(R.id.image_view_trivia);
        TextView textView = view.findViewById(R.id.text_view_trivia);

        imageView.setImageResource(image.get(position));
        textView.setText(text.get(position));
        return view;
    }

}
